--|--------------------------------------------------------
--|    Problem 10   |   https://projecteuler.net/problem=10
--|--------------------------------------------------------
--|
--| The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
--| 
--| Find the sum of all the primes below two million.
--|

prime = (num) ->
	for i = 2, num ^ (1 / 2)
		if num % i == 0
			false
	true

killme = (x) ->
	sum = 0
	for i = 2, x
		if prime(i)
			sum += i
	sum

print(killme(2000000))

