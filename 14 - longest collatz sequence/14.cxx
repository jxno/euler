/*	problem 14	~	https://projecteuler.net/problem=14
	---------------------------------------------------
	The following iterative sequence is defined for the set of positive integers:

	n → n/2 (n is even)
	n → 3n + 1 (n is odd)

	Using the rule above and starting with 13, we generate the following sequence:
	13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1

	It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. Although it has not been proved yet (collatz Problem), it is thought that all starting numbers finish at 1.

	Which starting number, under one million, produces the longest chain?

	NOTE: Once the chain starts the terms are allowed to go above one million.
*/

/*
	soooo the general idea is like this:
		- we use a template with params a bool called odd, an iterator, and x i guess.
		- that goes through the whole collatz sequence until it hits a certain breakpoint
		- we define that breakpoint in another template where `iter == 1`
		-
		- after that we recur until we generate the whole sequence!!
		-
		- next is the hard part of actually inputting different values into
		  collatz<number>::value since it's a metaprogram and not a runtime function
		- and this is where i get stuck -_-

		Hi dummy it's you from the future telling you to kiss charley, like a lot xx

		^ this dumb slut right here i stfg-
*/

/* \\ with template metaprogramming!!! // */

// give the main template defining our params and how / what to inf. recur
template <unsigned x, unsigned iter, bool odd>
struct collatzgz {};

// breakpoint ~ when x hits 1 we've reached the end of the sequence
template <unsigned iter>
struct collatzgz <iter, 1, true> {
	enum { value = iter };
};

// define the specialisation for even numbers
template <unsigned x, unsigned iter>
class collatzgz <iter, x, false> : public collatzgz <iter + 1, x / 2, (x/2) % 2> {};

// define the specialisation for odd numbers
template <unsigned x, unsigned iter>
class collatzgz <iter, x, true> : public collatzgz <iter + 1, x * 3 + 1, (x * 3 + 1) % 2> {};

template <unsigned x>
class collatz : public collatzgz <1, x, x % 2> {};

/* i have no idea what this code does
#include "type_traits"
template<typename T, typename Functor, typename U = typename std::result_of<Functor(T)>::type>
std::vector<U> v(const std::vector<T> &v, Functor&& f);
*/

// did all of that without iostream im so proud :>
#include "iostream"
#include "functional"
#include "numeric"

using namespace std;
auto main(void) -> int {
	cout << collatz<13>::value << endl;
	return 0;
}
