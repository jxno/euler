Program Problem1;
{$mode ObjFPC}

uses crt;

(* Problem One  |   https://projecteuler.net/problem=1
-------------------------------------------------------

    If we list all the natural numbers below 10
    that are multiples of 3 or 5, we get:

                3, 5, 6 and 9.
    
    The sum of these multiples is 23.

    Find the sum of all the multiples of 3 or 5 below 1000.

*)

const {Constants to change for interoprability.}
  div1 = 3;
  div2 = 5;
  max = 1000;

function is_multiple(x, a, b: integer): boolean;
{Check if the test case (x) is divisible by either divisor - a or b.}
begin
  {This is a really simple check & can be done on one line. Implicitly returns a boolean.}
  is_multiple := ((x mod div1) = 0) or ((x mod div2) = 0);
end;

var i : integer;
var result : uint32;

begin
  ClrScr;
  writeln('Project Euler - Problem One <3');

  {Start by testing all nums up to 10, then to 1000.}
  repeat
    if is_multiple(i, div1, div2) then
    begin
      {Uncomment this line to see the debug view - each number that is added to result.}
      {writeln('Execute MOD[',i,']');}
      result := result + i;
    end;   
    {Increment our iterator - the number we test the divisors for by one regardless of the outcome.}
    i := i + 1;
  until (i = max);

  {We've found our result! Now exit the program. <3}
  writeln('RESULT: ', result);
  writeln('Press any key to exit...');
  readkey;
end.
