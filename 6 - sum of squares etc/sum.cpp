/**	Problem 6	~	https://projecteuler.net/problem=6
 *	--------------------------------------------------
 *	The difference between the sum of the squares of the first ten natural numbers 
 *	and the square of the sum is: 3025 - 385 = 2640
 *
 *	Find the difference between the sum of the squares 
 *	of the first one hundred natural numbers and the square of the sum.
*/

#include "iostream"
constexpr uint32_t sumsquared (uint16_t x) {
	// x * (x + 1) * ( x * 2 + 1 ) / 6
	return ( x * (x + 1) * ( x * 2 + 1 ) ) / 6;
}

constexpr uint32_t squaresum (uint16_t x) {
	// (( x * ( x + 1 ) ) / 2) ^ 2
	return  (x * ( x + 1 )  *  (x * ( x + 1 ) )) / 4;
}

using namespace std;
auto main (void) -> int {
	cout << squaresum(100) - sumsquared(100) << endl;
	return 0;
}
