Program Problem20;
{$mode ObjFPC}

uses crt, sysutils, math;


(* Problem 20 | https://projecteuler.net/problem=20
---------------------------------------------------
  
  n! means n × (n − 1) × ... × 3 × 2 × 1
  
  For example, 10! = 10 × 9 × ... × 3 × 2 × 1 = 3628800,
  and the sum of the digits in the number 10! is:
  
        3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.
  
  Find the sum of the digits in the number 100!

*)

type Natural = 0..MaxInt;

function factorial(n: Natural): integer;
  var
    i : integer;
    fact : integer;
begin
  if n = 0 then factorial := 1
  else begin
    repeat 
      fact := fact * i;
      i := i + 1;
    until i > n;
  end;
end;

var ftoStr : string;
var facto, iter : uint64;
var c : char;

begin
  ClrScr;
  facto := factorial(100);
  fToStr := IntToStr(facto);

  for c in fToStr do
  begin
    iter := iter + Ord(c)
  end;

  writeln('Problem 20 <3');
  writeln('-------------');
  writeln(facto);
  writeln('RESULT: ', iter);
  writeln('Press any key to continue...');
  readkey;
end.