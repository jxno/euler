--    Problem 9

--  A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
--  a2 + b2 = c2
--  
--  For example, 32 + 42 = 9 + 16 = 25 = 52.
--  
--  There exists exactly one Pythagorean triplet for which a + b + c = 1000.
--  Find the product abc.

py = (x) ->
	for a = 1, x
		for b = 1, a
			c = math.sqrt(a ^ 2 + b ^ 2)
			if a + b + c == 1000
				print("answer:", a * b * c)

print py(1000)
