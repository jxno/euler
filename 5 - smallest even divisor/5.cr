module Fünf
  VERSION = "0.1.0"
  <<-DOC

  DOC

  def smallestmultiple(x)
    n = 0
    i = 0
    until n == x
      i += 1
      if n % i != 0
        break
      elsif i == x
        return n
      end
    end
    return n
  end

  p smallestmultiple(20)
end
